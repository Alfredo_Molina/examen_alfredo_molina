var bcrypt = require('bcryptjs');
var con = require('../config/conexion').mysql_pool;

var sqlMed = "INSERT INTO medico(id_medico, rut,num_verificador,nombres,apellidos,fecha_contratacion,num_celular,valor_consulta,id_especialidad,email,password) VALUES ?";
var valuesMed = [
    [1, '20166559', '0', 'Maxine Burt', 'Jade Mcgowan', '2010-7-14', '56983016689', '1800', '5', '', ''],
    [2, '28644298', '6', 'Venus Compton', 'Unity Tillman', '2011-05-28', '56932484112', '1300', '6', '', ''],
    [3, '49581944', '2', 'Rachel Angelica', 'Potter Battle', '2010-7-14', '56900847706', '1500', '20', '', ''],
    [4, "23649251", "6", "Benjamin Rudyard", "Walsh Cortez", "2012-02-14", "56989996826", 7327, "1 ", "", ""],
    [5, "17435341", "7", "Fritz Ethan", "Stephenson Livingston", "2008-02-02", "56918800021", 11305, "2 ", "", ""],
    [6, "39589031", "K", "Peter Nathan", "Martinez Webb", "2016-12-21", "56944362940", 8194, "3 ", "", ""],
    [7, "30620972", "8", "Paul Jolene", "Conley Grimes", "2004-02-11", "56921480897", 14967, "4 ", "", ""],
    [8, "8031611", "9", "Gabriel Stuart", "Dickerson Finch", "2008-11-14", "56936277551", 11160, "3 ", "", ""],
    [9, "38538518", "8", "Dahlia Yvette", "Sanford Ellison", "2015-01-19", "56978453873", 13208, "8 ", "", ""],
    [10, "32673703", "8", "Helen Beau", "Whitfield Joseph", "2010-11-27", "56984577534", 10079, "9 ", "", ""],
    [11, "27940770", "9", "Wallace Whoopi", "Hanson Velez", "2007-01-10", "56902623703", 9672, "7 ", "", ""],
    [12, "23568436", "5", "Chandler Tallulah", "Mcdaniel Mccray", "2012-01-31", "56993137428", 13604, "5 ", "", ""],
    [13, "15544380", "4", "Bertha Ima", "Moses Burke", "2014-08-27", "56960250056", 14780, "3 ", "", ""],
    [14, "9034215", "0", "McKenzie September", "Harrington Hammond", "2013-01-10", "56927349240", 10352, "11 ", "", ""],
    [15, "7504331", "7", "Laura Hayden", "Russell Cabrera", "2014-05-04", "56954341007", 9179, "9 ", "", ""],
    [16, "46268479", "7", "Keane Silas", "Sparks Robles", "2006-03-17", "56938267660", 12199, "7 ", "", ""],
    [17, "48011175", "3", "Quemby Alice", "Guzman Travis", "2011-01-20", "56957495435", 11846, "5 ", "", ""],
    [18, "44852315", "2", "Kaden Winter", "Hampton Brady", "2007-02-01", "56982921357", 11571, "3 ", "", ""],
    [19, "17123957", "5", "Damon Zorita", "Morton Sexton", "2012-08-07", "56970931991", 8563, "2 ", "", ""],
    [20, "10090796", "8", "Russell Yuli", "Gilliam Burgess", "2013-01-17", "56950295531", 9163, "9 ", "", ""],
    [21, "27306217", "3", "Keegan Iona", "Wheeler Mcintosh", "2016-12-21", "56935951156", 7126, "7 ", "", ""],
    [22, "19311627", "2", "Whitney Maris", "Mcgee Trujillo", "2006-11-21", "56918623525", 10139, "5 ", "", ""],
    [23, "47241730", "4", "Mark Elvis", "Whitley Chang", "2013-03-27", "56959727853", 10998, "3 ", "", ""],
    [24, "35805871", "K", "Harriet Len", "Glass Parrish", "2009-06-10", "56968451514", 12664, "1 ", "", ""],
    [25, "17807506", "3", "Shafira Lillian", "Sweeney Gallagher", "2006-09-04", "56903083198", 12420, "9 ", "", ""],
    [26, "38870738", "0", "Murphy Angelica", "Berg Higgins", "2013-12-21", "56990931820", 10417, "7 ", "", ""],
    [27, "11434665", "9", "Mia Dale", "Solis Shaffer", "2010-04-26", "56926498505", 10059, "5 ", "", ""],
    [28, "5417520", "5", "Laura Jolie", "Richard Hull", "2008-05-05", "56987648662", 8088, "3 ", "", ""],
    [29, "24647578", "4", "Byron Dean", "Haley Rogers", "2015-05-26", "56999674247", 12345, "2 ", "", ""],
    [30, "40933344", "3", "Denise Slade", "Salas Estes", "2007-02-04", "56949781552", 11920, "9 ", "", ""],
    [31, "10620246", "K", "Madaline Bruce", "Simmons Davidson", "2009-03-24", "56961209591", 12683, "7 ", "", ""],
    [32, "33612376", "3", "Zia Trevor", "Harrison Gill", "2006-10-23", "56929176069", 13246, "5 ", "", ""],
    [33, "47248367", "6", "Caesar Destiny", "Tate Harper", "2006-05-05", "56938303600", 11837, "3 ", "", ""],
    [34, "44524981", "5", "Dominique Blaine", "Oneil Cote", "2015-10-10", "56909049914", 14251, "1 ", "", ""],
    [35, "48073718", "0", "Fletcher Amelia", "Sharp Briggs", "2011-03-25", "56999756780", 12527, "9 ", "", ""],
    [36, "45272529", "0", "Fitzgerald Myra", "Ferguson Frederick", "2014-03-05", "56966721701", 8108, "7 ", "", ""],
    [37, "6265337", "K", "Alexandra Chiquita", "Mayo Lancaster", "2011-11-14", "56998979596", 14707, "5 ", "", ""],
    [38, "26969295", "2", "Clinton Hanae", "Green Benjamin", "2002-11-03", "56936578037", 7660, "3 ", "", ""],
    [39, "7807529", "5", "Kennedy Gwendolyn", "Key Bradshaw", "2013-12-01", "56998647375", 12948, "21 ", "", ""],
    [40, "34999118", "7", "Kennan Winifred", "Mccarthy Clark", "2016-12-11", "56940209619", 11857, "19", "", ""],
    [41, "11490106", "7", "Denise Bruno", "Goodman Henderson", "2013-08-25", "56908243084", 11881, "27", "", ""],
    [42, "5436625", "6", "Orla Prescott", "Bush Malone", "2006-05-05", "56925199644", 13433, "15", "", ""],
    [43, "22726327", "K", "Shafira Sydney", "Cooper Espinoza", "2009-12-17", "56920834820", 11710, "13", "", ""],
    [44, "40624827", "5", "Macy Duncan", "Rivas Newman", "2012-11-27", "56986853947", 13883, "21", "", ""],
    [45, "32224018", "K", "Alyssa Micah", "Waters Byrd", "2014-11-26", "56986575338", 11717, "28", "", ""],
    [46, "46317953", "0", "Jane Xanthus", "Mcdaniel Deleon", "2005-02-17", "56949920598", 7979, "17", "", ""],
    [47, "31475634", "7", "Juliet Nita", "Zamora Patton", "2016-03-10", "56926869075", 13490, "5", "", ""],
    [48, "49767157", "4", "Felix Stewart", "Holland Howell", "2008-03-05", "56963692389", 11510, "23", "", ""],
    [49, "13779545", "0", "Jamalia Bradley", "Terry Blake", "2008-09-07", "56994702230", 12519, "26", "", ""],
    [50, "47786092", "3", "Eleanor Wanda", "Malone Mcdaniel", "2015-11-03", "56931317543", 10292, "19", "", ""],
    [51, "38899751", "6", "Gloria Lyle", "Rowland Cantrell", "2006-10-13", "56990146078", 7161, "6 ", "", ""],
    [52, "19914273", "9", "Bradley Connor", "Mullins Wynn", "2006-02-19", "56993335031", 7004, "4 ", "", ""],
    [53, "33897726", "3", "Edward Garth", "Walton Hunt", "2006-05-02", "56967690740", 9896, "7 ", "", ""],
    [54, "34925369", "0", "Hedy Harriet", "Morrow Heath", "2014-05-17", "56937005713", 12668, "14", "", ""],
    [55, "7634224", "5", "Basia Meghan", "Cooley Johns", "2011-06-04", "56913346886", 14354, "25", "", ""],
    [56, "8456503", "2", "Risa Maya", "Hubbard Mcguire", "2014-07-18", "56975314684", 11218, "22", "", ""]

];
con.getConnection(function (err, connection) {
    if (err) throw err;
    console.log("Connectado!");

    connection.query(sqlMed, [valuesMed], function (err, result) {
        if (err) throw err;
        console.log("Medicos insertados");
        con.end();
        console.log("Conexion cerrada.");
    });
});