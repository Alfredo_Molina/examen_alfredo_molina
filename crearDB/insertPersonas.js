var bcrypt = require('bcryptjs');
var con = require('../config/conexion').mysql_pool;

bcrypt.genSalt(10, function (err, salt) {
    bcrypt.hash("1234", salt, function (err, hash) {

        var sqlEmp = "INSERT INTO empleado(id_empleado, rut,num_verificador,nombres,apellidos,fecha_contratacion,num_celular,id_cargo,email,password) VALUES ?";
        var valuesEmp = [
            [1, '28561838', 'K', 'Stewart Geoffrey', 'Lee Perez', '2008-7-04', '56956717360', '1', 'director@hotmail.com', hash],
            [2, '28644298', '6', 'Damian Lewis', 'Houston Cervantes', '2009-04-08', '56932484112', '2', 'Administrador@hotmail.com', hash],
            [3, '49581944', '2', 'Nichole Jillian', 'Brown Craft', '2016-12-14', '56900847706', '3', 'Secretaria@hotmail.com', hash]
        ];


        var sqlPac = "INSERT INTO paciente(id_paciente, rut,num_verificador,nombres,apellidos,fecha_nac,sexo,direccion,num_celular,num_contacto,tipo,email,password) VALUES ?";
        var valuesPac = [
            [1, '8140232', '9', 'Christian Jhon', 'Sampson Berger', '1970-7-04', 'Masculino', 'Maipu 5 abril 3154', '56953706628', '56907298157', 'paciente', '', ''],
            [2, '22825278', '6', 'Jolie Jane', 'Elliott Dhoe', '1973-04-08', 'Femenino', 'Est Central Las Rejas 614', '56906753869', '56995165167', 'paciente', '', ''],
            [3, '17092831', '8', 'Alexandra Tana', 'Brown Chang', '1986-12-14', 'Femenino', 'Santiago Alameda 4589', '56946535530', '56917545731', 'paciente', '', '']
        ];

        con.getConnection(function (err, connection) {
            if (err) throw err;
            console.log("Connectado!");

            connection.query(sqlEmp, [valuesEmp], function (err, result) {
                if (err) throw err;
                console.log("Empleados insertados");
            });

            connection.query(sqlPac, [valuesPac], function (err, result) {
                if (err) throw err;
                console.log("Pacientes insertados");
                con.end();
                console.log("Conexion cerrada.");
            });

        });

    });
});
