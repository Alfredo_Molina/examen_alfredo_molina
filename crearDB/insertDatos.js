var con = require('../config/conexion').mysql_pool;

var sqlEsp = "INSERT INTO especialidad (id_especialidad, nombre_esp) VALUES ?";
var valuesEsp = [
    [1, 'Alergia E Inmunología'],
    [2, 'Broncopulmonar'],
    [3, 'Cardiología'],
    [4, 'Coloproctología'],
    [5, 'Dermatología'],
    [6, 'Diabetes Y Nutrición'],
    [7, 'Endocrinología'],
    [8, 'Fonoaudiología'],
    [9, 'Gastroenterología'],
    [10, 'Geriatría'],
    [11, 'Ginecología'],
    [12, 'Hematología'],
    [13, 'Infectología'],
    [14, 'Kinesiología'],
    [15, 'Medicina Interna'],
    [16, 'Nefrología'],
    [17, 'Neurocirugia'],
    [18, 'Neurología'],
    [19, 'Nutricionista'],
    [20, 'Oftalmología'],
    [21, 'Oncología'],
    [22, 'Otorrinolaringología'],
    [23, 'Pediatría'],
    [24, 'Psicología'],
    [25, 'Psiquiatría'],
    [26, 'Reumatología'],
    [27, 'Traumatología'],
    [28, 'Urología']
];

var sqlCargo = "INSERT INTO cargo (id_cargo, nombre_cargo) VALUES ?";
var valuesCargo = [
    [1, 'Director'],
    [2, 'Administrador'],
    [3, 'Secretaria']
];

var sqlEstado = "INSERT INTO estado (id_estado, nombre_estado, descripcion) VALUES ?";
var valuesEstado = [
    [1, 'Agendada', 'Se ha confirmado una fecha para la consulta'],
    [2, 'Confirmada', '2 días antes de la atención el sistema debe alertar para que se proceda a la confirmación de la atención agendada'],
    [3, 'Anulada', 'Si el día antes la atención no ha sido confirmada el sistema debe anularla automática. También hay anulación manual por motivos particulares.'],
    [4, 'Perdida', 'Una atención se pierde si habiendo sido confirmada el paciente no acude a la misma, lo cual debe ser realizado manualmente o automáticamente por el sistema si a un día posterior a la fecha de atención la misma no ha sido marcada como realizada.'],
    [5, 'Realizada', 'La consulta se realizo sin problemas'],
];



con.getConnection(function (err, connection) {
    if (err) throw err;
    console.log("Connectado!");

    connection.query(sqlEsp, [valuesEsp], function (err, result) {
        if (err) throw err;
        console.log("Especialidades insertadas");
    });
    connection.query(sqlCargo, [valuesCargo], function (err, result) {
        if (err) throw err;
        console.log("Cargos insertados");
    });
    connection.query(sqlEstado, [valuesEstado], function (err, result) {
        if (err) throw err;
        console.log("Estados insertados");
        con.end();
        console.log("Conexion cerrada.");
    });

});


