var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var flash = require('connect-flash');
var session = require('express-session');
var expressValidator = require('express-validator');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var empleados = require('./routes/empleados');
var routes = require('./routes/index');
var users = require('./routes/users');


//view engine configuracion
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: false,
    resave: false
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
            , root = namespace.shift()
            , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// connectar Flash
app.use(flash());

// variables globales
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error_aux = req.flash('error_aux');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;//
    res.locals.session = req.session;//
    next();
});

app.use('/users', users);
app.use('/empleados', empleados);
app.use('/', routes);


//redireccion a not found si no encuentra la direccion
app.get('*', function (req, res) {
    res.render('pages/notFound');
});
//redireccion a pagina status 500 (errores relacionados al servidor)
app.get('/500', function (req, res) {
    res.render('500');
});

app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function () {
    console.log('Servidor iniciado en el puerto ' + app.get('port'));
});