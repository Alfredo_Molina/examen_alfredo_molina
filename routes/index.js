var express = require('express');
var router = express.Router();
var Rutjs = require('rutjs');
var passport = require('passport');
require('../config/passport')(passport);

router.get('/', function (req, res) {
    res.render('pages/index');
});

router.get('/login', function (req, res) {
    res.render('pages/login');
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    badRequestMessage: 'Ingrese todos los datos',
    failureFlash: true
}), function (req, res) {
    res.redirect('/');
});

router.get('/logout', function (req, res) {
    req.logout();
    req.flash('success_msg', 'Has cerrado Sesion');
    res.redirect('/login');
});


module.exports = router;
