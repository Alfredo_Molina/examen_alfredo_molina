var express = require('express');
var router = express.Router();
var passport = require('passport');
var con = require('../config/conexion').mysql_pool;

//Passport configuracion (exported)
require('../config/passport')(passport);

//Controladores
var medico_controller = require('../controllers/medico_controller');
var paciente_controller = require('../controllers/paciente_controller')
var consulta_controller = require('../controllers/consulta_controller')


router.get('/empelados', function (req, res) {
    res.render('pages/admin/emp-index');
});

//Post y Get realacionado a MEDICOs
router.get('/medicoCreate', medico_controller.medico_create_get);
router.post('/nuevoMed', medico_controller.medico_create_post);
router.get('/deleteMed/:id', medico_controller.medico_delete);
router.get('/medicoList', medico_controller.medico_list);
router.get('/medicoDetails/', medico_controller.medico_detail_get);
router.post('/medicoDetallesPost', medico_controller.medico_detail_post);

//Post y Get realacionado a Pacientes
router.get('/pacienteCreate', paciente_controller.paciente_create_get);
router.post('/nuevoPaciente', paciente_controller.paciente_create_post);
router.get('/eliminarPaciente/:id', paciente_controller.paciente_delete);
router.get('/pacienteList', paciente_controller.paciente_list);
router.get('/pacienteDetails/', paciente_controller.paciente_detail_get);
router.post('/pacienteDetallesPost', paciente_controller.paciente_detail_post);

//POST Y GET relacionado a consultas
router.get('/consultaCreate', consulta_controller.consulta_create_get);
router.post('/nuevaConsulta', consulta_controller.consulta_create_post);
router.get('/eliminarConsulta/:id', consulta_controller.consulta_delete);
router.get('/consultaList', consulta_controller.consulta_list);

router.get('/consultaDetails', consulta_controller.consulta_detail_get);

router.post('/consultaDetallesPost', consulta_controller.consulta_detail_post);
router.post('/consultaUpdatePost', consulta_controller.consulta_update_post);

//Deserializa el usuario para ser logeado (continuacion modulo Passport)
passport.deserializeUser(function (req, email, done) {
    con.query("SELECT * FROM empleado WHERE email = ?", [email], function (err, rows) {
        done(err, rows[0]);
    });
});

//funciones que validan que el tipo de empleado este logeado
//id_cargo 1 = Director
//id_cargo 2 = Administrador
//id_cargo 3 = Secretaria
function isLoggedDirector(req, res, next) {
    if (!req.isAuthenticated()) {
        res.redirect('/login');
    } else if (req.user.tipo === 'paciente') {
        res.redirect('/');
    } else {
        if (req.user.id_cargo == 1) {
            return next();
        }
    }
}
function isLoggedAdmin(req, res, next) {
    if (!req.isAuthenticated()) {
        res.redirect('/login');
    } else if (req.user.tipo === 'paciente') {
        res.redirect('/');
    } else {
        if (req.user.id_cargo == 2) {
            return next();
        }
    }
}
function isLoggedSecretaria(req, res, next) {
    if (!req.isAuthenticated()) {
        res.redirect('/login');
    } else if (req.user.tipo === 'paciente') {
        res.redirect('/');
    } else {
        if (req.user.id_cargo == 3) {
            return next();
        }
    }
}

module.exports = router;