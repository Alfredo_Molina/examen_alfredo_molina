var express = require('express');
var router = express.Router();
var passport = require('passport');
var con = require('../config/conexion').mysql_pool;

//Passport configuracion (exported)
require('../config/passport')(passport);

//Controladores
var paciente_controller = require('../controllers/paciente_controller');

router.get('/profile', function (req, res) {
    res.render('pages/paciente/profile');
});

//Post y Get realacionado a Pacientes()
router.get('/register', paciente_controller.pacienteUser_create_get);
router.post('/registrar', paciente_controller.pacienteUser_create_post);


passport.deserializeUser(function (req, email, done) {
    con.query("SELECT * FROM paciente WHERE email = ?", [email], function (err, rows) {
        done(err, rows[0]);
    });
});


module.exports = router;