var mysql = require('mysql');

pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'centroMedico',
    multipleStatements: true,
    debug: false,
    connectionLimit: 20
});

pool.on('enqueue', function () {
    connection.release();
    console.log('Waiting for available connection slot');
});

module.exports.execute = function (query, callback) {
    pool.getConnection(function (err, connection) {
        if (err) { return callback(err, null); }
        else if (connection) {
            connection.query(query, function (err, rows, result, fields) {
                connection.release();
                //console.log('connection.release();');
                if (err) { return callback(err, null); }
                return callback(null, rows, result, fields);
            })
        }
        else { return callback("No hay conexiones disponebles", null); }
    });
};
module.exports.executeWithParams = function (query, param1, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err, null);
        } else if (connection) {
            connection.query(query, param1, function (err, rows, result, fields) {
                if (err) { return callback(err, null); }
                return callback(null, rows, connection.release(),console.log('pool-connection.release();'));
                connection.release();
                console.log('pool-connection.release();');
            })
        } else {
            return callback("No hay conexiones disponebles", null);
        }
    });
};


