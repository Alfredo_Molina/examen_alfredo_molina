var mysql = require('mysql');
var Sequelize = require('sequelize');
var DataTypes = require('sequelize/lib/data-types');
const connection = new Sequelize('centroMedico', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 10,
        min: 0,
        idle: 10000
    },
});

connection
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('empleado', {
        idEmpleado: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'id_empleado'
        },
        rut: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'rut'
        },
        numVerificador: {
            type: DataTypes.STRING(2),
            allowNull: false,
            field: 'num_verificador'
        },
        nombres: {
            type: DataTypes.STRING(30),
            allowNull: false,
            field: 'nombres'
        },
        apellidos: {
            type: DataTypes.STRING(30),
            allowNull: false,
            field: 'apellidos'
        },
        fechaContratacion: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'fecha_contratacion'
        },
        numCelular: {
            type: DataTypes.BIGINT,
            allowNull: false,
            field: 'num_celular'
        },
        idCargo: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'cargo',
                key: 'id_cargo'
            },
            field: 'id_cargo'
        },
        email: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'email'
        },
        password: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'password'
        }
    }, {
            tableName: 'empleado'
        });
};

connection.findAll().then(empleado => {
    console.log(empleado.get('email'));
});