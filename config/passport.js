var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcryptjs');
var con = require('../config/conexion').mysql_pool;

module.exports = function (passport) {
    passport.use('local', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, email, password, done) {
            con.getConnection(function (err, connection) {
                connection.query("SELECT * FROM paciente WHERE email = ?", [email], function (err, rows) {
                    if (err)
                        return done(err);
                    if (!rows.length > 0) {
                        connection.query("SELECT * FROM empleado WHERE email = ?", [email], function (err, rows) {
                            if (err)
                                return done(err);
                            if (!rows.length > 0) {
                                return done(null, false, { message: 'No se encontro cuenta asociada a este Email.' });
                            } else {
                                if (bcrypt.compareSync(password, rows[0].password)) {
                                    return done(null, rows[0]);
                                } else {
                                    return done(null, false, { message: 'Contraseña Incorrecta.' });
                                }
                            }

                        });

                    } else {

                        if (bcrypt.compareSync(password, rows[0].password)) {
                            return done(null, rows[0]);
                        } else {
                            return done(null, false, { message: 'Contraseña Incorrecta.' });
                        }
                    }
                });
                connection.release();
                console.log('passport connection.release()');
            })
        }
    )
    );
    passport.serializeUser(function (user, done) {
        done(null, user.email);
    });
}
