var mysql = require('mysql');

var conexion = {
    mysql_pool: mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: '1234',
        database: 'centroMedico'
    })
};

module.exports = conexion;