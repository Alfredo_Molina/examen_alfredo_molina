var con = require('../config/conexion').mysql_pool;
var bcrypt = require('bcryptjs');
var Rutjs = require('rutjs');
var conn = require('../config/db');
var comunasJSON = require('../models/comunas.json');

exports.pacienteUser_create_get = function (req, res) {
    var errors = "";
    res.render('pages/registrar', {
        errors: errors
    });
};

exports.pacienteUser_create_post = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var password2 = req.body.password2;
    var rutFormat = new Rutjs(req.body.rut_medico);
    //Queries
    var queryRut = 'SELECT rut,email FROM paciente WHERE rut = ' + rutFormat.rut + ' ';
    var queryEmail = 'SELECT email FROM paciente WHERE email = ?';
    var queryUpdate = 'UPDATE paciente SET password = ?,email = ? WHERE rut = ?';
    // validaciones
    req.checkBody('rut_medico', 'RUT requerido').notEmpty();
    req.checkBody('email', 'Email requerido').notEmpty();
    req.checkBody('email', 'Email no valido').isEmail();//.len(13, 30);
    req.checkBody('password', 'Contraseña requerida').notEmpty();
    req.checkBody('password2', 'Las Contraseñas no coniciden').equals(req.body.password);

    req.getValidationResult().then(function (errors) {
        if (!errors.isEmpty()) {
            res.render('pages/registrar', { errors: errors.array() });
        } else if (!rutFormat.isValid) {
            req.flash('error_msg', 'RUT Invalido');
            res.redirect('/users/register');
        } else {
            conn.execute(queryRut, function (err, rows, result, fields) {
                if (err) throw err;

                var aux = [];
                var valido = false;
                for (var i = 0; i < rows.length; i++) {
                    aux = rows[i].email;
                }

                if (!rows.length > 0) {
                    req.flash('error_msg', 'RUT No encontrado');
                    res.redirect('/users/register');
                } else {
                    console.log('HAY RUT ')
                    if (aux.length > 0) {
                        req.flash('error_msg', 'Ya existe una cuenta asociada a este RUT');
                        res.redirect('/users/register');
                    } else {
                        conn.executeWithParams(queryEmail, [email], function (err, rows, result, fields) {
                            if (err) throw err;
                            if (rows.length > 0) {
                                req.flash('error_msg', 'el Email ya esta en uso');
                                res.redirect('/users/register');
                            } else {
                                bcrypt.genSalt(10, function (err, salt) {
                                    bcrypt.hash(password, salt, function (err, hash) {
                                        conn.executeWithParams(queryUpdate, [hash, email, rutFormat.rut], function (err, result) {
                                            if (err) throw err;
                                            req.flash('success_msg', 'Ya estas registrado, ahora puede iniciar sesion!');
                                            res.redirect('/login');
                                        });
                                    });
                                });
                            }
                        });
                    }
                }
            });
        }
    })
};

exports.paciente_list = function (req, res) {
    var totalRec = 0;
    var pageSize = 9;
    var pageCount = 0;
    var start = 0;
    var currentPage = 1;
    var queryCountRows = 'select count(*) as numrows from paciente order by id_paciente asc';
    conn.execute(queryCountRows, function (err, countrows, fields) {
        if (err) throw err;
        totalRec = countrows[0]['numrows'];

        pageCount = Math.ceil(totalRec / pageSize);
        if (typeof req.query.page !== 'undefined') {
            currentPage = req.query.page;
        }
        if (currentPage > 1) {
            start = (currentPage - 1) * pageSize;
        }
        var sqlQuery = 'SELECT * FROM paciente order by id_paciente asc LIMIT ' + start + ' , ' + pageSize + ' ';
        conn.execute(sqlQuery, function (err, data, fields) {
            if (err) throw err;
            res.render('pages/admin/pacienteListar', { pacientes: data, pageSize: pageSize, pageCount: pageCount, currentPage: currentPage });
        });
    });
};
exports.paciente_detail_get = function (req, res) {

    res.render('pages/admin/pacienteDetalle', { paciente: 0 });
}

exports.paciente_detail_post = function (req, res) {

    

    var rut_paciente = req.body.rut_paciente;

    var consulta = false;
    var rut = String(rut_paciente);
    rutFormat = new Rutjs(rut_paciente);
    console.log('rutFormat ' + rutFormat.rut)
    console.log('rut_paciente ' + rut_paciente)

    var querySelect = 'select * from paciente where rut = ' + rutFormat.rut;
    var queryJoin = 'select * from paciente p join consulta c on p.id_paciente = c.id_paciente join estado e on c.id_estado = e.id_estado where p.rut = ' + rutFormat.rut;

    if (rutFormat.isValid) {
        conn.execute(querySelect, function (err, data) {
            if (err) throw err;
            if (data.length > 0) {
                conn.execute(queryJoin, function (err, rows) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        res.render('pages/admin/pacienteDetalle', { consulta: true, paciente: rows });
                    } else {
                        res.render('pages/admin/pacienteDetalle', { consulta: false, paciente: data });
                    }
                })
            } else {
                req.flash('error_aux', 'No se encontraron pacientes relaciondos a este RUT');
                res.redirect('/empleados/pacienteDetails');
            }
        });
    } else {
        req.flash('error_msg', 'El RUT ingresado no es valido');
        res.redirect('/empleados/pacienteDetails');
    }
};

exports.paciente_create_get = function (req, res) {
    var errors = "";
    var comunas = comunasJSON.regiones;
    var comunasArray = comunas[14].comunas;

    res.render('pages/admin/pacienteCrear', { comunas: comunasArray, errors: errors });
};

exports.paciente_create_post = function (req, res) {
    var nombre1 = req.body.nombre1 + ' ' + req.body.nombre2;
    var apellido1 = req.body.apellido1 + ' ' + req.body.apellido2;
    var rut_paciente = req.body.rut_paciente;
    var fecha_nac = req.body.fecha_nac;
    var optradio = req.body.optradio;
    var numCel = req.body.numCel;
    var numCon = req.body.numCon;
    var direccion = req.body.comuna + ' ' + req.body.calle + ' ' + req.body.numeracion;
    var comunas = comunasJSON.regiones;
    var comunasArray = comunas[14].comunas;
    var fechaNacimiento = new Date(fecha_nac);

    //validaciones express-validator
    req.checkBody('nombre1', 'Primer Nombre requerido').notEmpty();
    req.checkBody('apellido1', 'Apellido Paterno requerido').notEmpty();
    req.checkBody('numCel', 'Celular requerido').notEmpty();
    req.checkBody('rut_paciente', 'RUT requerido').notEmpty();

    rutFormat = new Rutjs(rut_paciente);


    var data = {
        rut: rutFormat.rut,
        num_verificador: rutFormat.checkDigit,
        nombres: nombre1,
        apellidos: apellido1,
        fecha_nac: fechaNacimiento,
        sexo: optradio,
        direccion: direccion,//
        num_celular: numCel,
        num_contacto: numCon,
        tipo: "paciente",
        email: "",//opcional
        password: ""//opcional
    };

    var queryPaciente = 'SELECT rut from paciente where rut = ' + rutFormat.rut;
    var queryInsert = 'INSERT INTO paciente SET ? ';

    req.getValidationResult().then(function (errors) {
        if (!errors.isEmpty()) {
            res.render('pages/admin/pacienteCrear', { comunas: comunasArray, errors: errors.array() });
        } else if (!rutFormat.isValid) {
            req.flash('error_msg', 'El RUT ingresado no es valido');
            res.redirect('/empleados/pacienteCreate')
        } else {
            conn.execute(queryPaciente, function (err, rows, fields) {
                if (rows.length > 0) {
                    req.flash('error_msg', 'Ya existe un paciente con este RUT');
                    res.redirect('/empleados/pacienteCreate')
                } else {
                    conn.executeWithParams(queryInsert, data, function (err, rows, fields) {
                        if (err) throw err;
                        req.flash('success_msg', 'Se agregado el paciente con exito!');
                        res.redirect('/empleados/pacienteCreate')
                    });
                }
            });
        }

    });
};

exports.paciente_delete = function (req, res) {
    var id_paciente = req.params.id;

    var sqlQuery = 'SELECT * from consulta c join paciente m on c.id_paciente = m.id_paciente where m.id_paciente = ' + id_paciente + ' ';
    var deleteQuery = 'DELETE FROM paciente WHERE id_paciente = ' + id_paciente + ' ';
    var deleteConsulta = 'DELETE FROM consulta WHERE id_paciente = ' + id_paciente + ' ';

    conn.execute(sqlQuery, function (err, rows, result, fields) {
        if (err) throw err;
        var valido = false;
        if (rows.length > 0) {
            for (var i = 0; i < rows.length; i++) {
                aux = rows[i].id_estado;
                if (rows[i].id_estado == '1' || rows[i].id_estado == '2') {
                    valido = true;
                    break;
                } else { valido = false; }
            }
            if (valido) {
                console.log('NO SE PUEDE BORRAR')
                req.flash('error_msg', 'No se puede eliminar pacientes que tengan consultas Pendientes');
                res.redirect('/empleados/pacienteList');
            } else {
                conn.execute(deleteConsulta, function (err, rows, result, fields) {
                    if (err) throw err;
                    conn.execute(deleteQuery, function (err, rows, result, fields) {
                        if (err) throw err;
                        req.flash('success_msg', 'Se ha eliminado al Medico del sistema.');
                        res.redirect('/empleados/pacienteList');
                        console.log('SE PUEDE BORRAR')
                    });
                })
            }
        } else {
            conn.execute(deleteQuery, function (err, rows, result, fields) {
                if (err) throw err;
                req.flash('success_msg', 'Se ha eliminado al paciente del sistema.');
                res.redirect('/empleados/pacienteList');
                console.log('SE PUEDE BORRAR')
            });
        }
    })
};

