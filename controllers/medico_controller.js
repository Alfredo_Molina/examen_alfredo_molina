var con = require('../config/conexion').mysql_pool;
var conn = require('../config/db');
var Rutjs = require('rutjs');



exports.medico_list = function (req, res) {
    var totalRec = 0;
    var pageSize = 9;
    var pageCount = 0;
    var start = 0;
    var currentPage = 1;
    var queryCountRows = 'select count(*) as numrows from medico order by id_medico asc';


    conn.execute(queryCountRows, function (err, countrows, fields) {
        if (err) throw err;
        totalRec = countrows[0]['numrows'];

        pageCount = Math.ceil(totalRec / pageSize);
        if (typeof req.query.page !== 'undefined') {
            currentPage = req.query.page;
        }
        if (currentPage > 1) {
            start = (currentPage - 1) * pageSize;
        }
        var aux = start + ',' + pageSize;
        var sqlQuery = 'SELECT * FROM medico c join especialidad e on c.id_especialidad = e.id_especialidad order by id_medico asc LIMIT  ' + aux;
        conn.executeWithParams(sqlQuery, function (err, rows, fields) {
            if (err) throw err;
            res.render('pages/admin/listarMed', { medicos: rows, pageSize: pageSize, pageCount: pageCount, currentPage: currentPage });
        });
    });
};
exports.medico_detail_get = function (req, res) {

    res.render('pages/admin/consultarMed', { medico: 0 });
  
}
exports.medico_detail_post = function (req, res) {
    var rut_medico = req.body.rut_medico;
    var url = req.originalUrl;
    rutFormat = new Rutjs(rut_medico);
    

    var querySelect = 'select * from medico m join especialidad es on m.id_especialidad = es.id_especialidad where rut = ' + rutFormat.rut;

    if (rutFormat.isValid) {
        conn.execute(querySelect, function (err, rows) {
            if (rows.length > 0) {
                res.render('pages/admin/consultarMed', { medico: rows })
            } else {
                req.flash('error_aux', 'No se encontro medicos con RUT relacionado');
                res.redirect('/empleados/medicoDetails');
            }
        })
    } else {
        req.flash('error_msg', ' El RUT ingresado no es valido');
        res.redirect('/empleados/medicoDetails');
    }
};

exports.medico_create_get = function (req, res) {
    var errors = "";
    var query = 'SELECT * FROM especialidad';
    conn.execute(query, function (err, rows, fields) {
        res.render('pages/admin/agregarMed', { especialidades: rows, errors: errors })
    });
};

exports.medico_create_post = function (req, res) {
    var nombre1 = req.body.nombre1 + ' ' + req.body.nombre2;
    var apellido1 = req.body.apellido1 + ' ' + req.body.apellido2;
    var apellido2 = req.body.apellido2;
    var rut_medico = req.body.rut_medico;
    var valorC = req.body.valorC;
    var numCel = req.body.numCel;
    var espec = req.body.especialidad;
    //validaciones express-validator
    req.checkBody('nombre1', 'Primer Nombre requerido').notEmpty();
    req.checkBody('apellido1', 'Apellido Paterno requerido').notEmpty();
    req.checkBody('apellido2', 'Apellido Materno requerido').notEmpty();
    req.checkBody('valorC', 'Valor requerido').notEmpty();
    req.checkBody('numCel', 'Celular requerido').notEmpty();
    req.checkBody('rut_medico', 'RUT requerido').notEmpty();

    rutFormat = new Rutjs(rut_medico);
    var fechaActual = new Date();

    var data = {
        rut: rutFormat.rut,
        num_verificador: rutFormat.checkDigit,
        nombres: nombre1,
        apellidos: apellido1,
        fecha_contratacion: fechaActual,
        num_celular: numCel,
        valor_consulta: valorC,
        id_especialidad: espec,
        email: "",//opcional
        password: ""//opcional
    };

    var queryEespe = 'SELECT * FROM especialidad';
    var queryMedico = 'SELECT rut from medico where rut = ' + rutFormat.rut;
    var queryInsert = 'INSERT INTO medico SET ? ';
    console.log(queryInsert)
    req.getValidationResult().then(function (errors) {
        conn.execute(queryEespe, function (err, rows, fields) {
            if (!errors.isEmpty()) {
                res.render('pages/admin/agregarMed', { especialidades: rows, errors: errors.array() });
            } else if (!rutFormat.isValid) {
                req.flash('error_msg', 'El RUT ingresado no es valido');
                res.redirect('/empleados/,medicoCreate')
            } else {
                conn.execute(queryMedico, function (err, rows, fields) {
                    if (rows.length > 0) {
                        req.flash('error_msg', 'Ya existe un Medico con este RUT');
                        res.redirect('/empleados/medicoCreate')
                    } else {
                        //conn.executeWithParams(queryInsert, data, function (err, rows, fields) {
                            if (err) throw err;
                            req.flash('success_msg', 'Se agregado el medico con exito!');
                            res.redirect('/empleados/medicoCreate')
                       // });
                    }
                });
            }
        });
    });
};

exports.medico_delete = function (req, res) {
    var idMedico = req.params.id

    var sqlQuery = 'SELECT * from consulta c join medico m on c.id_medico = m.id_medico where m.id_medico = ' + idMedico + ' ';
    var deleteQuery = 'DELETE FROM medico WHERE id_medico = ' + idMedico + ' ';
    var deleteConsulta = 'DELETE FROM consulta WHERE id_medico = ' + idMedico + ' ';
    conn.execute(sqlQuery, function (err, rows, result, fields) {
        if (err) throw err;
        var valido = false;
        if (rows.length > 0) {
            for (var i = 0; i < rows.length; i++) {
                aux = rows[i].id_estado;
                if (rows[i].id_estado == '1' || rows[i].id_estado == '2') {
                    valido = true;
                    break;
                } else { valido = false; }
            }
            if (valido) {
                console.log('NO SE PUEDE BORRAR')
                req.flash('error_msg', 'No se puede despedir Medicos que tengan consultas Pendientes');
                res.redirect('/empleados/medicoList');
            } else {
                conn.execute(deleteConsulta, function (err, rows, result, fields) {
                    if (err) throw err;
                    conn.execute(deleteQuery, function (err, rows, result, fields) {
                        if (err) throw err;
                        req.flash('success_msg', 'Se ha eliminado al Medico del sistema.');
                        res.redirect('/empleados/medicoList');
                        console.log('SE PUEDE BORRAR')
                    });
                })
            }
        } else {
            conn.execute(deleteQuery, function (err, rows, result, fields) {
                if (err) throw err;
                req.flash('success_msg', 'Se ha eliminado al Medico del sistema.');
                res.redirect('/empleados/medicoList');
                console.log('SE PUEDE BORRAR')
            });
        }
    })
};
