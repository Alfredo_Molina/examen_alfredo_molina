var con = require('../config/conexion').mysql_pool;
var conn = require('../config/db');
var Rutjs = require('rutjs');

var sqlQuery1 = 'select con.id_consulta, con.fecha_atencion, con.id_paciente, con.id_estado, con.id_medico, con.id_empleado, con.observacion,con.fecha_creacion,  '
var sqlQuery2 = 'est.id_estado, est.nombre_estado, est.descripcion, '
var sqlQuery3 = 'CONCAT(emp.rut,emp.num_verificador) as rut_emp, emp.nombres as emp_nombres, emp.apellidos as emp_apellidos, emp.fecha_contratacion, emp.num_celular as emp_celular, '
var sqlQuery4 = 'pac.id_paciente as pac_id,CONCAT(pac.rut,pac.num_verificador) as rut_pac, pac.nombres as pac_nombres, pac.apellidos as pac_apellidos, pac.fecha_nac, pac.sexo, pac.direccion, pac.num_celular as pac_celular, pac.num_contacto as pac_num_contacto, '
var sqlQuery5 = 'med.id_medico as med_id,CONCAT(med.rut,med.num_verificador) as rut_med, med.nombres as med_nombres, med.apellidos as med_apellidos, med.fecha_contratacion, med.num_celular as med_celular, med.valor_consulta, '
var sqlQuery6 = 'esp.id_especialidad, esp.nombre_esp '
var sqlQuery7 = 'from consulta con join estado est on con.id_estado = est.id_estado  join empleado emp on con.id_empleado = emp.id_empleado '
var sqlQuery8 = 'join paciente pac on con.id_paciente = pac.id_paciente join medico med on con.id_medico = med.id_medico  join especialidad esp '
var sqlQuery9 = 'on med.id_especialidad = esp.id_especialidad '

exports.consulta_list = function (req, res) {
    var totalRec = 0;
    var pageSize = 9;
    var pageCount = 0;
    var start = 0;
    var currentPage = 1;
    var queryCountRows = 'select count(*) as numrows from consulta order by id_consulta asc';
    conn.executeWithParams(queryCountRows, function (err, countrows, fields) {
        if (err) throw err;
        totalRec = countrows[0]['numrows'];

        pageCount = Math.ceil(totalRec / pageSize);
        if (typeof req.query.page !== 'undefined') {
            currentPage = req.query.page;
        }
        if (currentPage > 1) {
            start = (currentPage - 1) * pageSize;
        }
        //por alguna razon el Stored Procedure no devolvia datos, asi que tube que hacer una Query completa de toda la DB
        var sqlQueryAux = 'order by con.id_consulta asc LIMIT ' + start + ' , ' + pageSize + ' ';
        var sqlQuery = sqlQuery1 + sqlQuery2 + sqlQuery3 + sqlQuery4 + sqlQuery5 + sqlQuery6 + sqlQuery7 + sqlQuery8 + sqlQuery9 + sqlQueryAux;

        conn.executeWithParams(sqlQuery, function (err, data, fields, result) {
            if (err) throw err;
            res.render('pages/admin/consultaListar', { consultas: data, pageSize: pageSize, pageCount: pageCount, currentPage: currentPage });
        });
    });
};

exports.consulta_detail_get = function (req, res) {
    
    res.render('pages/admin/consultaDetalle', { consultas: 0 });
}

exports.consulta_detail_post = function (req, res) {
    var id_consulta = req.body.id_consulta;

    var sqlQueryAux = 'where con.id_consulta = ? '
    var sqlQuery = sqlQuery1 + sqlQuery2 + sqlQuery3 + sqlQuery4 + sqlQuery5 + sqlQuery6 + sqlQuery7 + sqlQuery8 + sqlQuery9 + sqlQueryAux;


    conn.executeWithParams(sqlQuery, [id_consulta], function (err, rows) {
        if (err) throw err;
        if (rows.length > 0) {
            res.render('pages/admin/consultaDetalle', { consultas: rows })
        } else {
            req.flash('error_aux', ' No se econtraron consultas');
            res.redirect('/empleados/consultaDetails');
        }
    })

}

exports.consulta_create_get = function (req, res) {
    var errors = "";
    var queryMedico = 'SELECT * FROM medico med join especialidad esp on med.id_especialidad = esp.id_especialidad order by id_medico asc';
    var queryPaciente = 'SELECT * FROM paciente order by id_paciente asc';
    conn.executeWithParams(queryMedico, function (err, dataMed) {
        if (err) throw err;
        conn.executeWithParams(queryPaciente, function (err, dataPac) {
            if (err) throw err;
            res.render('pages/admin/consultaCrear', { medicos: dataMed, pacientes: dataPac, errors: errors });
        })
    })

}

exports.consulta_create_post = function (req, res) {
    var id_paciente = req.body.id_paciente;
    var id_medico = req.body.id_medico;
    var observacion = req.body.observacion;
    var fecha_atencion = req.body.fecha_atencion;
    var hora_atencion = req.body.hora_atencion;
    //valudaciones
    req.checkBody('fecha_atencion', 'Fecha requerida').notEmpty();
    req.checkBody('hora_atencion', 'Hora requerido').notEmpty();

    var dd = new Date(fecha_atencion).getDate();
    var mm = new Date(fecha_atencion).getMonth() + 1;
    var yy = new Date(fecha_atencion).getFullYear();
    var aux = fecha_atencion + ' ' + hora_atencion;
    var fechaFinal = new Date(aux);

    var errors = "";

    // solo para ingresar pruebas sin logearse
    if (req.isAuthenticated()) {
        var id_empleado = req.user.id_empleado;
    } else {
        var id_empleado = 3;
    }

    var fecha = new Date;
    var code = fecha.getDay() + fecha.getSeconds().toLocaleString() + fecha.getMilliseconds();

    data = {
        id_consulta: code,
        fecha_atencion: fechaFinal,
        id_paciente: id_paciente,
        id_estado: 1,//por defecto queda en estado 1 = agendada
        id_medico: id_medico,
        id_empleado: id_empleado,
        observacion: observacion,
        fecha_creacion: fecha
    };
    var queryInsert = 'INSERT INTO consulta SET ? ';
    var querySelect = 'SELECT * FROM consulta where id_medico = ' + id_medico;
    req.getValidationResult().then(function (errors) {
        if (!errors.isEmpty()) {
            req.flash('error_msg', 'Hora y fecha requeridos');
            res.redirect('/empleados/consultaCreate')
        } else {
            conn.executeWithParams(querySelect, function (err, rows) {
                if (err) throw err;
                if (rows.length > 0) {
                    var fechaAgendada = new Date(rows[0].fecha_atencion)
                    if (fechaFinal.getTime() == fechaAgendada.getTime()) {
                        req.flash('error_msg', 'la hora ya ha sido agendada, eliga otro medico');
                        res.redirect('/empleados/consultaCreate')
                    } else {
                        conn.executeWithParams(queryInsert, [data], function (err, rows) {
                            if (err) throw err;
                            req.flash('success_msg', 'Se ha creado la consulta con exito!');
                            res.redirect('/empleados/consultaCreate')
                        })
                    }
                } else {
                    conn.executeWithParams(queryInsert, [data], function (err, rows) {
                        if (err) throw err;
                        req.flash('success_msg', 'Se ha creado la consulta con exito!');
                        res.redirect('/empleados/consultaCreate')
                    })
                }
            })
        }
    })
}

exports.consulta_delete = function (req, res) {
    var id_consulta = req.params.id;

    var sqlQuery = 'SELECT * FROM consulta WHERE id_consulta = ' + id_consulta + ' ';
    var deleteConsulta = 'DELETE FROM consulta WHERE id_consulta = ' + id_consulta + ' ';
    conn.executeWithParams(sqlQuery, function (err, rows, result, fields) {
        if (err) throw err;
        var valido = false;
        if (rows.length > 0) {
            for (var i = 0; i < rows.length; i++) {
                aux = rows[i].id_estado;
                if (rows[i].id_estado == '1' || rows[i].id_estado == '2') {
                    valido = true;
                    break;
                } else { valido = false; }
            }
            if (valido) {
                console.log('NO SE PUEDE BORRAR')
                req.flash('error_msg', 'No se puede eliminar consultas con estado Pendiente');
                res.redirect('/empleados/consultaList');
            } else {
                conn.executeWithParams(deleteConsulta, function (err, rows, result, fields) {
                    if (err) throw err;
                    req.flash('success_msg', 'Se ha eliminado la consulta del sistema.');
                    res.redirect('/empleados/consultaList');
                    console.log('SE PUEDE BORRAR')
                });
            }
        } else {
            conn.execute(deleteConsulta, function (err, rows, result, fields) {
                if (err) throw err;
                req.flash('success_msg', 'Se ha eliminado la consulta del sistema.');
                res.redirect('/empleados/consultaList');
                console.log('SE PUEDE BORRAR')
            });
        }
    })
};

exports.consulta_update_post = function (req, res) {
    var id_consulta = req.body.id_consulta;
    var id_estado = req.body.id_estado;
    var observacion = req.body.observacion;

    var queryUpdate = 'update consulta set id_estado = ?, observacion = ? where id_consulta = ?'

    conn.executeWithParams(queryUpdate, [id_estado, observacion, id_consulta], function (err, rows) {
        if (err) throw err;
        req.flash('success_msg', 'Estado consulta cambiado.');
        res.redirect('/empleados/consultaList');
        //res.redirect('/empleados/consultaDetailsGET/'+id_consulta);
    })
}