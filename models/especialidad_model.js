var con = require('../config/conexion').mysql_pool;


exports.load = function (selector, next) {
    con.getConnection(function (err, connection) {
        connection.query('SELECT * FROM especialidad WHERE ?', selector, function (err, vals) {
            if (err) {
                return next(err);
            }
            //at this point you could return a user object
            //next(new User(vals[0]));
            //or just return the array.
            next(null, vals);
        });
        connection.release();
    });
};

